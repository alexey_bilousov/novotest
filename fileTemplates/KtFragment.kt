package ${PACKAGE_NAME}

import com.riard.rpg.R
import com.riard.rpg.base.BaseFragment
import com.riard.rpg.databinding.Fragment${BASE_NAME}Binding

class ${BASE_NAME}Fragment : BaseFragment<${BASE_NAME}ViewModel, Fragment${BASE_NAME}Binding>() {
    override fun getViewModelClass() = ${BASE_NAME}ViewModel::class

    override fun getLayoutId() = R.layout.fragment_${RESOURCE_NAME}
}