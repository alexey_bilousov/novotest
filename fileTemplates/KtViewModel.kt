package ${PACKAGE_NAME}

import android.app.Application
import com.riard.rpg.base.BaseViewModel

class ${BASE_NAME}ViewModel(app: Application) : BaseViewModel<${BASE_NAME}Repository>(app) {

    init {
        repo = ${BASE_NAME}Repository()
    }
}