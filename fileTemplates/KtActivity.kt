package ${PACKAGE_NAME}

import com.riard.rpg.R
import com.riard.rpg.base.BaseActivity
import com.riard.rpg.databinding.ActivityMainBinding

class ${BASE_NAME}Activity : BaseActivity<${BASE_NAME}ViewModel, Activity${BASE_NAME}Binding>() {

    override fun getViewModelClass() = ${BASE_NAME}ViewModel::class

    override fun getLayoutId() = R.layout.activity_${RESOURCE_NAME}

}